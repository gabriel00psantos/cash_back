# Cash_back Implementation

## Instruções

#### Construa o ambiente

    docker-compose up --b --d

#### Rode as migrations

      docker-compose exec web flask db init
      docker-compose exec web flask db migrate
      docker-compose exec web flask db upgrade

#### Rode os testes

	docker-compose exec web coverage run --source=project -m unittest discover -s tests/

#### Gerando relatórios de cobertura

    docker-compose exec web coverage html                                                           

#### REST API

Se tudo deu certo, a um health-check estara disponivel no endereço http://localhost:5000/health-check

#### Com tudo rodando

exportar o arquivo Cash Back.postman_collection.json para o seu postman, se cadastrar, logar e usar os endpoints


