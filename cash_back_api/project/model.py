from flask_sqlalchemy import SQLAlchemy
from passlib.hash import pbkdf2_sha256
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
import enum

db = SQLAlchemy()


def configure(app):
    db.init_app(app)
    app.db = db


class Middleman(db.Model):
    __tablename__ = "middleman"
    #Set to String to avoid problems with sqlite db on tests
    middleman_id = db.Column(db.String(36), 
                                     default=lambda: str(uuid.uuid4()), 
                                     primary_key=True)

    cpf = db.Column(db.String(11), unique=True, nullable=False)
    fullname = db.Column(db.String(255), nullable=True)
    password = db.Column(db.String(255), nullable=False)

    def gen_hash(self):
        self.password = pbkdf2_sha256.hash(self.password)

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)


class PurchaseStatus(enum.Enum):
    waiting = "Em validação"
    approved = "Aprovado"

    def __str__(self):
        return self.value


class Purchase(db.Model):
    __tablename__ = "purchase"
    #Also Set to String to avoid problems with sqlite db on tests
    purchase_id = db.Column(db.String(36), 
                                     default=lambda: str(uuid.uuid4()), 
                                     primary_key=True)

    cpf = db.Column(db.String(11), db.ForeignKey('middleman.cpf'))

    status = db.Column(db.Enum(PurchaseStatus))
    value = db.Column(db.Float(), nullable=False)
    cashback_percent = db.Column(db.Float(), nullable=False)
    cashback_value = db.Column(db.Float(), nullable=False)

    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    middleman = db.relationship('Middleman', backref='middleman_purchase', lazy='select',
                                cascade="all, delete")
