from flask import current_app
import requests
import logging


logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


def make_request(cpf):
    headers = {
        'token': current_app.config['EXTERNAL_AUTH']
    }
    
    url = (current_app.config['EXTERNAL_URL']).format(cpf)
    try:
        r = requests.get(url=url, headers=headers)
        extras = {
            'status': r.status_code,
            'cpf_consultado': cpf

        }
        logging.info('Requisição realizada - {}'.format(extras), extra=extras)
        if r.status_code == 404:
            logging.info('Nenhum registro encontrado', extra=extras)
            raise Exception('Nenhum registro encontrado.')

        data = r.json()

        if r.status_code == 200:
            logging.error('Sucesso ao realizar Requisição', extra=extras)
            situacao = data['body']['credit']
            return {'total_value': situacao}, 200
        else:
            mensagem_erro = data['body'].get('mensagem', 'Erro desconhecido')
            logging.error('Erro ao realizar Requisição {}'.format(
                          mensagem_erro), extra=extras)
            payload = {'error': mensagem_erro}, 422
    except Exception as e:
        return {'error': str(e)}, 500

    
