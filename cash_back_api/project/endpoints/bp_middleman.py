import logging
from flask import Blueprint, request, jsonify, current_app
from datetime import timedelta
from flask_jwt_extended import create_access_token, create_refresh_token
from project.model import Middleman
from project.serializer import MiddlemanSchema, LoginSchema
from sqlalchemy.exc import SQLAlchemyError

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

bp_middleman = Blueprint('middleman', __name__)


@bp_middleman.route('/signin', methods=['POST'])
def register():
    try:
        us = MiddlemanSchema()
        user, error = us.load(request.json)
        if error:
            return {'error': error}, 401

        user.gen_hash()
        current_app.db.session.add(user)
        current_app.db.session.commit()
        return us.jsonify(user), 201
    except Exception as e:
        logging.error('Error on insert middleman {}'.format(str(e)))
        return {'error': 'Error on insert middleman'}, 500


@bp_middleman.route('/login', methods=['POST'])
def login():
    user, error = LoginSchema().load(request.json)

    if error:
        return {'error': error}, 401

    user = Middleman.query.filter_by(cpf=user.cpf).first()

    if user and user.verify_password(request.json['password']):
        acess_token = create_access_token(
            identity=user.cpf,
            expires_delta=timedelta(days=1)
        )

        return jsonify({
            'acess_token': acess_token,
            'message': 'Success :)'
        }), 200

    return jsonify({
        'message': 'Invalid credentials'
    }), 401
