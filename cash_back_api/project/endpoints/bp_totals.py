from flask import Blueprint, request, current_app, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from marshmallow.exceptions import ValidationError
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql import func
import logging

from project.serializer import PurchaseSchema
from project.model import Purchase
from project.utils import make_request

bp_total = Blueprint('totals', __name__)


@bp_total.route("/user_total/", methods=['GET'])
@jwt_required
def user_total():
    user_cpf = get_jwt_identity()
    user_total = make_request(user_cpf)
    return user_total


@bp_total.route("/estimated_total/", methods=['GET'])
@jwt_required
def estimated_total():
    user_cpf = get_jwt_identity()
    estimated_total = Purchase.query.with_entities(
        func.sum(Purchase.cashback_value).label('estimated_total')
    ).filter(Purchase.cpf == user_cpf).first()[0]

    if not estimated_total:
        estimated_total = 0

    data = {
        'estimated_total': estimated_total
    }

    return data, 200
