from flask import Blueprint, request, current_app, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from marshmallow.exceptions import ValidationError
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql import func
import logging

from project.serializer import PurchaseSchema
from project.model import Purchase

bp_purchase = Blueprint('purchase', __name__)


@bp_purchase.route("/purchase/", methods=['GET'])
@jwt_required
def listOneAll():
    cSchema = PurchaseSchema(many=True)
    p_id = request.args.get('purchase_id')
    if p_id:
        result = Purchase.query.filter(Purchase.purchase_id == p_id)
    else:
        result = Purchase.query.all()

    data = cSchema.jsonify(result)
    status = 404 if not data else 200
    return data, status


@bp_purchase.route("/purchase/", methods=['POST'])
@jwt_required
def create():
    cSchema = PurchaseSchema()
    request.json['cpf'] = get_jwt_identity()
    try:
        payload, error = cSchema.load(request.json)
        if error:
            return {'error': error}, 422

        current_app.db.session.add(payload)
        current_app.db.session.commit()
        result = cSchema.jsonify(payload)
    except SQLAlchemyError as e:
        current_app.db.session.rollback()
        return jsonify({'error': 'Data related error'}), 422
    except Exception as e:
        return {'error': str(e)}, 422
    else:
        return result, 200
