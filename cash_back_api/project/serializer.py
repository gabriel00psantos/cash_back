from marshmallow import fields, post_load, validate
from flask_marshmallow import Marshmallow
from project.model import Middleman, Purchase, PurchaseStatus

ma = Marshmallow()


def configure(app):
    ma.init_app(app)


class LoginSchema(ma.Schema):
    class Meta:
        model = Middleman

    cpf = fields.Str(required=False)
    fullname = fields.Str(required=False)
    password = fields.Str(required=True)

    @post_load
    def make_middleman(self, data, **kwargs):
        return Middleman(**data)


class MiddlemanSchema(LoginSchema):
    fullname = fields.Str(required=True)


class PurchaseSchema(ma.Schema):
    class Meta:
        model = Purchase
        strict = True

    purchase_id = fields.Str(dump_only=True, attribute="purchase_id")
    value = fields.Float(required=True)
    cpf = fields.Str(required=True)
    status = fields.Str(dump_only=True)
    cashback_percent = fields.Float(dump_only=True)
    cashback_value = fields.Float(dump_only=True)
    created_at = fields.Date(dump_only=True)

    @post_load
    def make_purchase(self, data, **kwargs):
        purchase = Purchase(**data)
        if purchase.value < 1000:
            purchase.cashback_percent = 10
        elif purchase.value <= 1500:
            purchase.cashback_percent = 15
        else:
            purchase.cashback_percent = 20

        purchase.cashback_value = (
            purchase.value * (purchase.cashback_percent/100))

        purchase.cpf = data['cpf']
        if purchase.cpf == 15350946056:
            purchase.status = PurchaseStatus.approved
        else:
            purchase.status = PurchaseStatus.waiting
        return purchase
