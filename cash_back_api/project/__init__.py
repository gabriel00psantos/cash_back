import os

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for
)

from flask_cors import CORS
from flask_jwt_extended import JWTManager
from .model import Middleman, Purchase, configure as config_db
from .serializer import configure as config_ma
from .endpoints.bp_purchase import bp_purchase
from .endpoints.bp_totals import bp_total
from .endpoints.bp_middleman import bp_middleman

def create_app():
    app = Flask(__name__)
    app.config.from_object("project.config.Config")
    CORS(app)
    config_db(app)
    config_ma(app)
    JWTManager(app)
    Migrate(app, app.db)

    app.register_blueprint(bp_middleman)
    app.register_blueprint(bp_purchase)
    app.register_blueprint(bp_total)

    @app.shell_context_processor
    def inject_models():
        return {
            'Middleman': Middleman,
            'Purchase': Purchase
        }

    @app.route('/health-check', methods=['GET'])
    def health():
        return {"Cash-back": ":D"}

    return app
