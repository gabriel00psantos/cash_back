from flask import url_for
from .flask_base_tests_cases import TestFlaskBase
from datetime import datetime


class TestPurchaseInsert(TestFlaskBase):
    def test_purchase_insert_ok(self):
        self.create_user()
        token = self.create_token()
        data = {"value": 4500}
        expected = {
            "value": 4500,
            "cashback_percent": 20,
            "cashback_value": 900,
            "status": "Em validação"
        }

        response = self.client.post(url_for('purchase.create'),
                                    json=data,
                                    headers=token)
        self.assertEqual(expected['value'], 
                         response.json['value'])
        
        self.assertEqual(expected['cashback_percent'], 
                         response.json['cashback_percent'])
        
        self.assertEqual(expected['cashback_value'], 
                         response.json['cashback_value'])

        self.assertEqual(expected['status'], 
                         response.json['status'])

    def test_purchase_without_fields(self):
        self.create_user()
        token = self.create_token()
        dado = {}

        expected = {
            "error": "{'value': ['Missing data for required field.']}"
        }

        response = self.client.post(
            url_for('purchase.create'), headers=token, json=dado)
        self.assertEqual(expected, response.json)
        self.assertEqual(response.status_code, 422)

