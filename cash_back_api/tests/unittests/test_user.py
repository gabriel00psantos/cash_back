from flask import url_for
from .flask_base_tests_cases import TestFlaskBase


class TestUserBP(TestFlaskBase):
    def test_register_users_ok(self):
        user = {
            'cpf': '43467102808',
            'fullname': 'Pedro Silva',
            'password': 'pass@123'
        }

        esperado = {
            'cpf': '43467102808',
            'fullname': 'Pedro Silva'
        }
        response = self.client.post(url_for('middleman.register'), json=user)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json['fullname'], esperado['fullname'])
        self.assertEqual(response.json['cpf'], esperado['cpf'])

    def test_register_users_error_validation(self):
        user = {
            'cpf': '464646464',
            'fullname': 'Pedrinho pedro',
        }

        esperado = {'error': {'password': ['Missing data for required field.']}}
        response = self.client.post(url_for('middleman.register'), json=user)

        self.assertEqual(response.status_code, 401)

        self.assertEqual(response.json, esperado)

    def test_login_user_bad_credentials(self):
        user = {
            'cpf': '464646464',
            'fullname': 'test',
            'password': '1234'
        }

        esperado = {
            'cpf': '464646464',
            'fullname': 'test'
        }
        response = self.client.post(url_for('middleman.register'), json=user)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json['cpf'], esperado['cpf'])

        wrong_pass = {'password': 'senha123'}
        user.update(wrong_pass)

        response_login = self.client.post(url_for('middleman.login'), json=user)
        self.assertEqual(response_login.status_code, 401)





