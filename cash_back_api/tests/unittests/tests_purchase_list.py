from flask import url_for
from .flask_base_tests_cases import TestFlaskBase
from datetime import datetime


class TestShowPurchase(TestFlaskBase):
    def test_show_empty_purchase_query(self):
        self.create_user()
        token = self.create_token()
        response = self.client.get(
            url_for('purchase.listOneAll'),
            headers=token
        )
        self.assertEqual([], response.json)

    def test_show_single_purchase_inserted(self):
        self.create_user()
        token = self.create_token()
        data = {"value": 100}

        response = self.client.post(url_for('purchase.create'), headers=token,
                                    json=data)
        response = self.client.get(
            url_for('purchase.listOneAll'), headers=token)
        self.assertEqual(1, len(response.json))

    def test_show_by_purchase_id(self):
        self.create_user()
        token = self.create_token()
        data = {"value": 100}

        response = self.client.post(
            url_for('purchase.create'), json=data, headers=token)
        
        response = self.client.get(url_for('purchase.listOneAll',
                                           purchase_id=response.json['purchase_id']),
                                   headers=token)
        self.assertEqual(1, len(response.json))
        self.assertEqual(data['value'], response.json[0]['value'])

