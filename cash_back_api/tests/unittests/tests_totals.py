from flask import url_for
from .flask_base_tests_cases import TestFlaskBase
from datetime import datetime


class TestTotalsReferences(TestFlaskBase):
    def test_estimated_total(self):
        self.create_user()
        token = self.create_token()
        data = {"value": 4500}
        expected = {
            "value": 4500,
            "cashback_percent": 20,
            "cashback_value": 900,
            "status": "Em validação"
        }

        response = self.client.post(url_for('purchase.create'),
                                    json=data,
                                    headers=token)
        self.assertEqual(expected['value'], 
                         response.json['value'])
        
        self.assertEqual(expected['cashback_percent'], 
                         response.json['cashback_percent'])
        
        self.assertEqual(expected['cashback_value'], 
                         response.json['cashback_value'])

        self.assertEqual(expected['status'], 
                         response.json['status'])

        response = self.client.get(url_for('totals.estimated_total'),
                                   headers=token)

        self.assertEqual(expected['cashback_value'], 
                         response.json['estimated_total'])

    def test_external_total(self):
        self.create_user()
        token = self.create_token()
        
        response = self.client.get(url_for('totals.user_total'),
                                   headers=token)

        self.assertEqual(int, 
                         type(response.json['total_value']))

        self.assertEqual(response.status_code, 200)

